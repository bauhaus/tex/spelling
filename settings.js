module.exports = {
  internal: {
    spelling: {
      host: process.env.LISTEN_ADDRESS || "0.0.0.0",
    },
  },
};
