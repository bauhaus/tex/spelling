FROM node:10-alpine as base

WORKDIR /app

FROM base as app

RUN apk add --no-cache git
RUN git clone https://gitlab.com/bauhaus/tex/mirror/spelling.git /app/

RUN npm ci --quiet

FROM base

RUN apk add --no-cache tini aspell aspell-en aspell-de aspell-fr
#RUN apk add --no-cache aspell aspell-af aspell-am  aspell-ar-large aspell-bg aspell-bn aspell-br aspell-ca aspell-cs aspell-cy aspell-da aspell-de aspell-el aspell-eo aspell-es aspell-et aspell-eu-es aspell-fa aspell-fo aspell-fr aspell-ga aspell-gl-minimos aspell-gu aspell-he aspell-hi aspell-hr aspell-hsb aspell-hu aspell-hy aspell-id aspell-is aspell-it aspell-kk aspell-kn aspell-ku aspell-lt aspell-lv aspell-ml aspell-mr aspell-nl aspell-nr aspell-ns  aspell-pa aspell-pl aspell-pt aspell-pt-br aspell-ro aspell-ru aspell-sk aspell-sl aspell-ss aspell-st aspell-sv aspell-tl aspell-tn aspell-ts aspell-uk aspell-uz aspell-xh aspell-zu

WORKDIR /tmp
RUN apk add --no-cache make
COPY install-deps.sh languages.txt ./
RUN ./install-deps.sh
# eu kk nr ns st ts xh

WORKDIR /app

COPY --from=app /app /app
COPY ./settings.js /app/config/settings.js
ENV SHARELATEX_CONFIG=/app/config/settings.js

RUN mkdir -p cache && echo "[]" > cache/spell.cache && chown -R node:node cache
USER node

ENTRYPOINT ["/sbin/tini", "--"]

CMD ["node", "--expose-gc", "app.js"]