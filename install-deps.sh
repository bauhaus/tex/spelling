#!/usr/bin/env sh

mkdir -p /tmp/aspell

while read L; do
  echo $L
  cd /tmp/aspell
  wget "$L"
  tar xvf aspell*
  cd aspell* && ./configure && make && make install
  rm -rf /tmp/aspell/aspell*
done < /tmp/languages.txt